namespace Exemplo2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdicionaErros : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "url", c => c.String(maxLength: 8));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "url");
        }
    }
}
