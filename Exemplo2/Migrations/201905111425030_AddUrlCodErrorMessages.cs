namespace Exemplo2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUrlCodErrorMessages : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "codigo", c => c.String(nullable: false, maxLength: 8));
            AlterColumn("dbo.Products", "url", c => c.String(maxLength: 80));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "url", c => c.String(maxLength: 8));
            AlterColumn("dbo.Products", "codigo", c => c.String(nullable: false));
        }
    }
}
